output "istio-LoadBalancer" {
  value = data.kubernetes_service.istio-ingressgateway.load_balancer_ingress[0].ip
}

output "dns_name" {
  value = google_dns_record_set.dns.name
}

output "dns_rrdata" {
  value = google_dns_record_set.dns.rrdatas
}

output "dns_wildcard_name" {
  value = google_dns_record_set.dns_wildcard.name
}

output "dns_wildcard_rrdata" {
  value = google_dns_record_set.dns_wildcard.rrdatas
}