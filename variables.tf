variable "DNS_MANAGED_ZONE" {
  default = "techtur-bid"
}
//variable "DOMAIN_NAME" {
//  default = "techtur.bid"
//}
variable "CLUSTER_NAME" {}
variable "GCP_DNS_PROJECT" {
  default = "techtur-bid-commons"
}
variable "wait_for_resources" {}