resource "null_resource" "wait_for_resources" {
  triggers = {
    depends_on = join("", var.wait_for_resources)
  }
}

data "kubernetes_service" "istio-ingressgateway" {
  depends_on = [null_resource.wait_for_resources]
  metadata {
    name = "istio-ingressgateway"
    namespace = "istio-system"
  }
}

data "google_dns_managed_zone" "techtur-bid" {
  depends_on = [null_resource.wait_for_resources]

  project = var.GCP_DNS_PROJECT
  name        = var.DNS_MANAGED_ZONE
}

resource "google_dns_record_set" "dns" {
  depends_on = [null_resource.wait_for_resources]

  project = var.GCP_DNS_PROJECT
  name = "${var.CLUSTER_NAME}.${data.google_dns_managed_zone.techtur-bid.dns_name}"
  type = "A"
  ttl  = 300

  managed_zone = "${data.google_dns_managed_zone.techtur-bid.name}"

  rrdatas = ["${data.kubernetes_service.istio-ingressgateway.load_balancer_ingress.0.ip}"]
}

resource "google_dns_record_set" "dns_wildcard" {
  depends_on = [null_resource.wait_for_resources]

  project = var.GCP_DNS_PROJECT
  name = "*.${var.CLUSTER_NAME}.${data.google_dns_managed_zone.techtur-bid.dns_name}"
  type = "A"
  ttl  = 300

  managed_zone = "${data.google_dns_managed_zone.techtur-bid.name}"

  rrdatas = ["${data.kubernetes_service.istio-ingressgateway.load_balancer_ingress.0.ip}"]
}