Techturbid Terraform Registry - istio_dns
---
#### DNS module used by techturbid's GKE modules like gke_base

This module will a DNS record with the cluster name as a subdomain of the `techtur.bid`
 
---

The input required for this modules are:

|            **Variable**           |       **Value**       |
|-----------------------------------|:----------------------|
| CLUSTER_NAME                      |                       |

--- 
 
The variables with default values are:

|      **Variable**     |       **Value**     |
|-----------------------|:--------------------|
| DNS_MANAGED_ZONE      | techtur-bid      |
| CLOUDDNS_PROJECT      | techtur-bid-commons |

---

This module will output the following values:

|              **Output**            |                       **Value**               |
|------------------------------------|:----------------------------------------------|
| istio-load_balancer-ip                   | data.kubernetes_service.istio-ingressgateway.load_balancer_ingress[0].ip               |
| dns_name                | google_dns_record_set.dns.name |
| dns_rrdata       | google_dns_record_set.dns.rrdatas            |
| dns_wildcard_name      | google_dns_record_set.dns_wildcard.name             |
| dns_wildcard_rrdata      | google_dns_record_set.dns_wildcard.rrdatas             |

---

This module requires the following providers which will be loaded by gke_base or other similar module:
```hcl-terraform
provider "google" {
  credentials = var.service-account_key
  project     = var.gcp_project
  region      = var.cluster_region
}

provider "kubernetes" {
  load_config_file = false
  host = "https://${module.gke.cluster_endpoint}"
  cluster_ca_certificate = base64decode(module.gke.cluster_ca_certificate)
  token = data.google_client_config.google-beta_current.access_token
}
```

---